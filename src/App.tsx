import React, {
  useState,
  useEffect,
  useMemo,
  useRef,
  useCallback,
} from "react";
import "ag-grid-enterprise";
import { AgGridReact } from "ag-grid-react";
import thousand from "./thousand.csv";
import tenThousand from "./tenThousand.csv";
import fiftyThousand from "./fiftyThousand.csv";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import Papa from "papaparse";
import "./styles.css";
import Select from "react-select";
import starIcon from "./star_FILL0_wght400_GRAD0_opsz48.svg";
import { nanoid } from "nanoid";
import MultiselectEditor from "./multiselectEditor";

function App() {
  const [columnDefs, setColumnDefs] = useState([
    {
      field: "first",

      // editable: true,
      filter: "agTextColumnFilter",
      filterParams: {
        buttons: ["reset", "apply"],
      },
      headerName: "NFT Name",
      headerComponentParams: {
        template: ` <div class="ag-cell-label-container" role="presentation">
        <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" aria-hidden="true"></span>
        <div ref="eLabel" class="ag-header-cell-label" role="presentation">
        <img src=${starIcon} width=15></img>
            <span ref="eText" class="ag-header-cell-text"></span>
            <span ref="eFilter" class="ag-header-icon ag-header-label-icon ag-filter-icon" aria-hidden="true"></span>
            <span ref="eSortOrder" class="ag-header-icon ag-header-label-icon ag-sort-order" aria-hidden="true"></span>
            <span ref="eSortAsc" class="ag-header-icon ag-header-label-icon ag-sort-ascending-icon" aria-hidden="true"></span>
            <span ref="eSortDesc" class="ag-header-icon ag-header-label-icon ag-sort-descending-icon" aria-hidden="true"></span>
            <span ref="eSortNone" class="ag-header-icon ag-header-label-icon ag-sort-none-icon" aria-hidden="true"></span>
        </div>
    </div>`,
      },
      wrapText: true,
      enableRowGroup: true,
      autoHeight: true,
      rowDrag: true,
      headerCheckboxSelection: true,
    },
    {
      field: "last",
      editable: true,
      headerName: "Owner",
      cellEditorPopup: true,
      cellEditor: MultiselectEditor,
      cellRenderer: (params: any) => <span>{params.value}</span>,
    },
    {
      field: "age",
      headerName: "Price",
      editable: true,
      cellStyle: (params: any) => {
        if (params.value > 24) {
          return {
            backgroundColor: "lightblue",
          };
        }
      },
    },
    {
      field: "province",
      cellEditor: "agRichSelectCellEditor",
      editable: true,
      headerName: "Properties",
      cellEditorPopup: true,
      cellEditorParams: {
        cellHeight: 20,
        values: [
          "English",
          "Spanish",
          "French",
          "Portuguese",
          "Georgian",
          "Arabian",
          "Russian",
          "Chinese",
        ],
      },
    },
    { field: "gender", editable: true, headerName: "MediaType" },
    { field: "phone", editable: true, headerName: "Media link" },
    {
      field: "ccnumber",
      headerName: "Minted amount",
      sortable: true,
      aggFunc: (params: any) => {
        const sum = params.values.reduce(
          (prev: number, next: number) => prev + next,
          0
        );
        return sum / params.values.length;
      },
    },
    { headerName: "Supply", field: "state" },
    { headerName: "Status", field: "street" },
    { headerName: "Description", field: "paragraph" },
    {
      field: "",
      suppressNavigable: true,
      cellClass: "no-border",
      cellRenderer: (params: any) => <button>...</button>,
    },
  ]);
  const gridRef: any = useRef({});
  const [rowData, setRowData] = useState();
  const [loading, setLoading] = useState(false);
  const [cellBeingEdited, setCellBeingEdited] = useState(false);
  const [prevNode, setPrevNode] = useState({
    guid: null,
    colId: null,
  });

  const defaultColDef = useMemo(
    () => ({
      sortable: true,
      enableRowGroup: true,
    }),
    []
  );

  useEffect(() => {
    Papa.parse(tenThousand, {
      download: true,
      header: true,
      complete: (data) => {
        setRowData(data.data.slice(0, -1) as any);
        setLoading(false);
      },
    });
  }, []);

  const handleAddColumn = (e: any) => {
    let column: any = {};
    if (e.target.value === "text") {
      column = {
        field: "text",
        editable: "true",
      };
    } else {
      column = {
        field: "select",
        editable: "true",
        cellEditor: "agSelectCellEditor",
        cellEditorParams: {
          values: [1, 2],
        },
      };
    }

    gridRef.current.api.setColumnDefs([...columnDefs, column]);
    setColumnDefs((state) => [...state, column]);
  };

  const onCellClicked = (e: any) => {
    setPrevNode({
      colId: e.column.colId,
      guid: e.data.guid,
    });

    if (e.column.colId === prevNode.colId && e.data.guid === prevNode.guid) {
      setCellBeingEdited(true);
      return;
    }

    if (!cellBeingEdited) {
      setCellBeingEdited(true);
    } else {
      gridRef.current.api.stopEditing();
      setCellBeingEdited(false);
    }
  };

  const processDataFromClipboard = useCallback(
    (params: any): string[][] | null => {
      const data = [...params.data];
      const emptyLastRow =
        data[data.length - 1][0] === "" && data[data.length - 1].length === 1;
      if (emptyLastRow) {
        data.splice(data.length - 1, 1);
      }
      const lastIndex = gridRef.current!.api.getModel().getRowCount() - 1;
      const focusedCell = gridRef.current!.api.getFocusedCell();
      const focusedIndex = focusedCell!.rowIndex;
      if (focusedIndex + data.length - 1 > lastIndex) {
        const resultLastIndex = focusedIndex + (data.length - 1);
        const numRowsToAdd = resultLastIndex - lastIndex;
        const rowsToAdd: any[] = [];
        for (let i = 0; i < numRowsToAdd; i++) {
          const index = data.length - 1;
          const row = data.slice(index, index + 1)[0];
          // Create row object
          const rowObject: any = {};
          let currentColumn: any = focusedCell!.column;
          row.forEach((item: any) => {
            if (!currentColumn) {
              return;
            }
            rowObject[currentColumn.colDef.field] = item;
            currentColumn =
              gridRef.current!.columnApi.getDisplayedColAfter(currentColumn);
          });
          rowObject.guid = nanoid();
          rowsToAdd.push(rowObject);
        }
        gridRef.current!.api.applyTransaction({ add: rowsToAdd });
      }
      return data;
    },
    []
  );

  return (
    <div style={{ display: "flex", alignItems: "flex-start" }}>
      <div className="ag-theme-alpine" style={{ height: 760, width: 1500 }}>
        <AgGridReact
          ref={gridRef as any}
          rowData={rowData}
          columnDefs={columnDefs}
          enableRangeSelection={true}
          enableFillHandle={true}
          defaultColDef={defaultColDef}
          getRowId={(params) => params.data.guid}
          rowDragManaged={true}
          singleClickEdit={true}
          // onCellClicked={onCellClicked}
          rowGroupPanelShow={"always"}
          suppressDragLeaveHidesColumns={true}
          suppressMakeColumnVisibleAfterUnGroup={true}
          suppressRowGroupHidesColumns={true}
          // processDataFromClipboard={processDataFromClipboard}
          undoRedoCellEditing={true}
          undoRedoCellEditingLimit={20}
        ></AgGridReact>
      </div>
      <div>
        <span>Add Column</span>
        <select
          name={"add-columns"}
          id={"add-columns"}
          onChange={handleAddColumn}
        >
          <option value={"text"}>Text</option>
          <option value={"select"}>Select</option>
        </select>
      </div>
    </div>
  );
}

export default App;
